# Web Gallery

## Application:

In this application, we can view photos from different artists.

The link to the application is:
[https://dhanasettyabhishek.gitlab.io/web_gallery_vue/](https://dhanasettyabhishek.gitlab.io/web_gallery_vue/)

In our web gallery, we can navigate to different sections and view photos from various photographers.

## Tech Stack used:

```
List of packages and libraries used are:
1. NodeJS (https://nodejs.org/en/docs/)
2. VueJS (https://vuejs.org/v2/guide/)
3. BootstrapVue(https://bootstrap-vue.js.org/docs/)
```

### Building the application:

```
Step 1: Create a Vue project using Vue CLI.
>vue create "Project Name."
Step 2: Add bootstrap components:
>vue add bootstrap-vue
Step 3: Import data from a JSON file (dataset), which contains the image URL and the artist name.
Step 4: Build, run, and test the application.
>npm run serve
Step 5: Deploy for production using NodeJs (npm)
>npm run build

*Additionally uploaded all the files to GitLab for deployment. Using continuous integration, I was able to deploy and test it @ https://dhanasettyabhishek.gitlab.io/web_gallery_vue/

```

All the code is available at [https://gitlab.com/dhanasettyabhishek/web_gallery_vue/-/tree/master/Code](https://gitlab.com/dhanasettyabhishek/web_gallery_vue/-/tree/master/Code)
